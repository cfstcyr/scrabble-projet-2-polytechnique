export const PLAYER_ALREADY_TRYING_TO_JOIN = 'A player is already trying to join the game';
export const NO_OPPONENT_IN_WAITING_GAME = 'No opponent is waiting for the game';
export const OPPONENT_NAME_DOES_NOT_MATCH = 'Opponent name does not match. Cannot accept game';
export const CANNOT_HAVE_SAME_NAME = 'Cannot join a game with a player with the same name';
