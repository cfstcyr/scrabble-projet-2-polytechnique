export const SOCKET_SERVICE_NOT_INITIALIZED = 'SocketService not initialized';
export const INVALID_ID_FOR_SOCKET = 'Invalid ID for socket';
